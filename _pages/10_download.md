---
layout: page
title: Download
permalink: /download/
order: 2
---


## Source Code

[Source code packages]({{ site.url }}/_pages/Download/Download_source.html) for the latest stable and
development versions of Graphviz are available, along with instructions for anonymous
access to the sources using [Git](http://git-scm.com/).

## Executable Packages

Packages marked with an asterisk(*) are provided by outside parties.
We list them for convenience, but disclaim responsibility for the contents of these packages.

### Linux

We do not provide precompiled packages any more.
You may find it useful to try one of the following third-party sites.

* [Ubuntu packages](https://packages.ubuntu.com/search?keywords=graphviz&searchon=names)*
* [Fedora project](https://apps.fedoraproject.org/packages/graphviz)*
* [Debian packages](http://packages.debian.org/search?suite=all&amp;searchon=names&amp;keywords=graphviz)*
* [Stable and development rpms for Redhat Enterprise, or Centos systems](http://rpmfind.net/linux/rpm2html/search.php?query=graphviz)* available but are out of date.
<!---  The problem is that this probably just points back to graphviz.org * [Fedora](http://fedoraproject.org/)* On a working Fedora system, use `yum list "graphviz*"` to see all available Graphviz packages.  --->

### Windows

* [Development Windows install packages](https://ci.appveyor.com/project/ellson/graphviz-pl238)
* [Stable 2.38 Windows install packages]({{ site.url }}/_pages/Download/Download_windows.html)
* [Cygwin Ports](http://sourceware.org/cygwinports/)* provides a port of Graphviz to Cygwin.
* [WinGraphviz](http://wingraphviz.sourceforge.net/wingraphviz/)* Win32/COM object (dot/neato library for Visual Basic and ASP).

Mostly correct notes for building Graphviz on Windows can be found
[here]({{ site.url }}/_pages/doc/winbuild.html). 

### Mac

* [MacPorts](https://www.macports.org/)* provides both stable and development versions of
Graphviz and the Mac GUI Graphviz.app. These can be obtained via the ports "graphviz", "graphviz-devel", "graphviz-gui" and "graphviz-gui-devel".
* [Homebrew](https://brew.sh/)* has a Graphviz port.

We need help with OSX, if you would like to volunteer.

We would appreciate if someone donates a script to
run pkgbuild or productbuild to automatically generate OSX installers.
[Packaging for Apple Administrators](https://itunes.apple.com/us/book/packaging-for-apple-administrators/id1173928620?mt=11&ign-mpt=uo%3D4)
could be a good reference. Also blog articles like [Creating OS X Package Files  (.pkg) in Terminal](http://techion.com.au/blog/2014/8/17/creating-os-x-package-files-pkg-in-terminal). Note graphviz needs postinstall actions, at least `dot -c`; also `fc-cache` if Graphviz has freetype/cairopango drivers. 

### Solaris

For Solaris, please use the graphviz stable releases [here](http://www.opencsw.org/packages/). These are
maintained by Laurent Blume / John Ellson. Currently available packages are:

<table>
     <tr><td>graphviz</td><td>Graph Visualization Tools</td></tr>
     <tr><td>graphviz_dev</td><td>Graphviz headers etc. for development</td></tr>
     <tr><td>graphvizdoc</td><td>Graphviz documentation</td></tr>
     <tr><td>graphvizgd</td><td>Graphviz renderers using gd</td></tr>
     <tr><td>graphvizgraphs</td><td>Graphviz example graphs</td></tr>
     <tr><td>graphvizguile</td><td>Graphviz language binding for guile</td></tr>
     <tr><td>graphvizperl</td><td>Graphviz language binding for perl</td></tr>
     <tr><td>graphvizpython</td><td>Graphviz language binding for python</td></tr>
     <tr><td>graphvizruby</td><td>Graphviz language binding for ruby</td></tr>
     <tr><td>graphvizsharp</td><td>Graphviz language binding for C#</td></tr>
     <tr><td>graphviztcl</td><td>Graphviz language binding for tcl</td></tr>
</table>

Minimally, graphviz and graphvizgd should be installed.

### Other Unix

* [FreeBSD](http://www.freshports.org/graphics/graphviz/)*






